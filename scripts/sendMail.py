from django.template import Context, Template
from django.utils import timezone
from django.conf import settings

from django.core.mail import EmailMultiAlternatives

from vglist.models import VGListUser

def run(filePath,subject):
	SUBJECT = subject

	now = timezone.now()
	template = Template(open(filePath).read())

	for vglistuser in VGListUser.objects.filter(user__is_active=True,can_receive_mail=True):
		context = Context({
			'username': vglistuser.user.username,
			'today': now,
			'DOMAIN_NAME': settings.DOMAIN_NAME,
		});
		
		msg = EmailMultiAlternatives(
			SUBJECT,
			template.render(context),
			'noreply@'+settings.DOMAIN_NAME,
			[vglistuser.user.email],
		)
		msg.attach_alternative(
			template.render(context),
			"text/html",
		)
		
		msg.send()
