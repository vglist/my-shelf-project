from django.template import Context, loader
from django.utils import timezone
from django.conf import settings

from django.core.mail import EmailMultiAlternatives

from vglist.models import VGListUser

SUBJECT = "The VGList Newsletter #0"

now = timezone.now()
template_html = loader.get_template('mail/DB_info__can_receive_mail.html')
template_txt = loader.get_template('mail/DB_info__can_receive_mail.txt')

for vglistuser in VGListUser.objects.filter(user__is_active=True):
	context = Context({
		'username': vglistuser.user.username,
		'today': now,
		'DOMAIN_NAME': settings.DOMAIN_NAME,
	});
	
	msg = EmailMultiAlternatives(
		SUBJECT,
		template_txt.render(context),
		'noreply@'+settings.DOMAIN_NAME,
		[vglistuser.user.email],
	)
	msg.attach_alternative(
		template_html.render(context),
		"text/html",
	)
	msg.send()
