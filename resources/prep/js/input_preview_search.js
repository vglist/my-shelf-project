window.autocomplete_timer = 0;
window.results_cache = {}

function results_popup(results, id){
	html = '';
	
	for(var i=0; i<results.result.length; i++){
		html += '<option value="'+results.result[i].pk+'">'+results.result[i].name+'</option>';
	}
	$('#'+id).html(html);
}

function get_input_list(input, type){
	clearTimeout(autocomplete_timer)
	autocomplete_timer = setTimeout(function(){
		if(input.value.length>=1){
			if(input.value in results_cache){
				results_popup(results_cache[input.value])
			}
			else{
				if(type=="publisher"){
					$.getJSON("{% url 'vglist.views.studios_by_prefix' %}", data={'prefix':input.value}, success=function(data){
						results_cache[input.value] = data		   
						results_popup(results_cache[input.value], type)
					})
				}
								
				if(type=="developers"){
					$.getJSON("{% url 'vglist.views.studios_by_prefix' %}", data={'prefix':input.value}, success=function(data){
						results_cache[input.value] = data		   
						results_popup(results_cache[input.value], type)
					})
				}
			}
		}
	},500)
}
