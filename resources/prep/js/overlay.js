$(document.body).delegate('#screen','click',function(e)
{
	this.classList.remove("show")
	this.classList.add("hide")
}).delegate('#screen > div','click',function(e)
{
	e.stopPropagation();
})

function show_overlay()
{
	$("#screen")[0].className = "show"
}

function hide_overlay()
{
	$("#screen")[0].className = "hide"
}

function get_overlay()
{
	return document.getElementById('screen')
}
