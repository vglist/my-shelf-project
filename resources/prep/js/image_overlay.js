$('.big_image').click(function(event){
	event.stopPropagation();
	$('.image_overlay').fadeIn();
	$('.image_overlay_back').fadeIn();
});

$('.image_overlay').click(function(event) {
	event.stopPropagation();
});

$('html').click(function(e) {
	$('.image_overlay').fadeOut();
	$('.image_overlay_back').fadeOut();
});
