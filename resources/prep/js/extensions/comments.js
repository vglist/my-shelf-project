/*	VGLIST'S COMMENTS PLUGIN

-- 	Author: Felipe Aguiar (zakkwylde)
--	
*/

{% load static %}

(function($){
	$.fn.vglistComments = function(comments, formURL, myAvatarURL, options){
		
		//plugin main code
		return $(this).each(function(){
			var rethtml = '';
			rethtml += '<div class="comments_form"> <div class="comments_form_header"><h2 class="comments_title">COMMENTS</h2><h2 class="commentsnum">';
			
			var today = new Date();
			var today_dd = today.getDate();
			var today_mm = today.getMonth()+1;
			var today_yyyy = today.getFullYear();
			
			var options_default = {
				'login_token':false
			}
			
			if(options == undefined){
				options = options_default;
			}
						
			if(comments.length > 0){
				rethtml+= comments.length + ' comments';
			}

			else{
				rethtml+='No comments here. Be the first to comment!';
			}
			
			rethtml += '</div></h2> \
				<div class="post_avatar"> \
					<img src="'+myAvatarURL+'" /> \
				</div> \
				\
				<div class="post_input_wrapper"> \
					<form method="POST" action="'+formURL+'">';
			
			if(options.login_token==true){
				rethtml+= '<textarea name="text" cols="5" placeholder="Type your response here... (Click to Expand)" id="input_comment_text" class="input_comment_text"></textarea>';
			}
			else{
				rethtml+= '<textarea disabled name="text" cols="5" placeholder="You can\'t comment if you are not logged in!" class="input_comment_text"></textarea>';
			}
				rethtml+='<p style="text-align:right"><input type="submit" value="Post" class="btn2_comments" id="reply_comments" /></p>\
					</form>\
				</div>\
			</div>';
			
			for(var msg = 0; msg < comments.length; msg++){
				rethtml += '<div class="comment_row">\
					<div class="comment_avatar">\
						<a href="'+ comments[msg].commenter_profile_url +'"><img src="'+ comments[msg].commenter_avatar +'" /></a>\
					</div>\
					<div class="comment_text_wrapper">\
						<div class="comment_text_title">\
						<a href="'+ comments[msg].commenter_profile_url +'" class="username">'+ comments[msg].commenter+'</a> <span class="bullet">&#8226;</span>'
						
						//Test if comment date is older than today
						//If true, shows date
						//Else, shows XXXX ago format
						if((comments[msg].comment_date.day < today_dd) && (comments[msg].comment_date.month <= today_mm) && (comments[msg].comment_date.year <= today_yyyy)){
							rethtml+='<span class="comment_date">'+ comments[msg].comment_date.month_text + ' ' + comments[msg].comment_date.day + ', ' + comments[msg].comment_date.year + ' @ '+comments[msg].comment_date.hour+':'+comments[msg].comment_date.minute+'</span><br>';
						}

						else if((comments[msg].comment_date.day > today_dd) && (comments[msg].comment_date.month < today_mm) && (comments[msg].comment_date.year <= today_yyyy)){
							rethtml+='<span class="comment_date">'+ comments[msg].comment_date.month_text + ' ' + comments[msg].comment_date.day + ', ' + comments[msg].comment_date.year + ' @ '+comments[msg].comment_date.hour+':'+comments[msg].comment_date.minute+'</span><br>';

						}
						
						else if((comments[msg].comment_date.day > today_dd) && (comments[msg].comment_date.month > today_mm) && (comments[msg].comment_date.year < today_yyyy)){
							rethtml+='<span class="comment_date">'+ comments[msg].comment_date.month_text + ' ' + comments[msg].comment_date.day + ', ' + comments[msg].comment_date.year + ' @ '+comments[msg].comment_date.hour+':'+comments[msg].comment_date.minute+'</span><br>';

						}
						
						else if((comments[msg].comment_date.day < today_dd) && (comments[msg].comment_date.month > today_mm) && (comments[msg].comment_date.year < today_yyyy)){
							rethtml+='<span class="comment_date">'+ comments[msg].comment_date.month_text + ' ' + comments[msg].comment_date.day + ', ' + comments[msg].comment_date.year + ' @ '+comments[msg].comment_date.hour+':'+comments[msg].comment_date.minute+'</span><br>';

						}
		
						else{
							rethtml+='<span class="comment_date">'+ comments[msg].comment_date.timesince +' ago</span><br>'							
						}
												
						rethtml+="</div>\
								\
								<div class='comment_text'>\
									"+comments[msg].comment_text+"\
								</div>\
								\
								<div class='comment_text_footer'>\
									<a class='reply' id='reply' to="+comments[msg].commenter+" ><img src=\"{% static 'icons/black312.png' %}\" /></a>\
								</div>\
							</div>\
						</div>";
			}

			rethtml+= '<div class="commentsarea_footer"><a href="#" id="back_to_top">BACK TO TOP &#923;</a></div>';
						
			$(document).click(function(e){
				var d = e.target;
				var f = e.target;
				var g = e.target; 
				
				// if this node is not the one we want, move up the dom tree
				while (d != null && d['id'] != 'reply_comments') {
				  d = d.parentNode;
				}
				
				// if this node is not the one we want, move up the dom tree
				while (f != null && f['id'] != 'input_comment_text') {
				  f = f.parentNode;
				}
				
				// if this node is not the one we want, move up the dom tree
				while (g != null && g['id'] != 'reply') {
				  g = g.parentNode;
				}
								
				var insideButton = (d != null && d['id'] == 'reply_comments');
				var insidePost = (f != null && f['id'] == 'input_comment_text');
				var insideReply = (g != null && g['id'] == 'reply');
				
				if(insideButton){
				}
				
				else if(insideReply){
					var antes = $('.input_comment_text').val();
					var nome = $(e.target).closest('.comment_row').find('.username').html();
					$('.input_comment_text').select().val(antes + ' @' + nome + " ");
					$("body, html").animate({
						scrollTop: $(".input_comment_text").position().top
					});
					showPostInput();
				}
				
				else if(insidePost){
					showPostInput();
				}
				
				else{
					hidePostInput($('.input_comment_text'));
				}
			});
			
			rethtml = $(rethtml);
			
			//SCROLL BACK TO INPUT
			rethtml.find("#back_to_top").click(function() {
				$("body, html").animate({
					scrollTop: $('body,html').position().top
				});	
			});	
			
			$(this).html(rethtml);
		});
		
	}
	
}(jQuery));

function showPostInput(){
	$('#input_comment_text').animate({ height: 150 }, 400);
}

function hidePostInput(textarea){
	$('#input_comment_text').animate({ height: 30 }, 400);			
}


		
