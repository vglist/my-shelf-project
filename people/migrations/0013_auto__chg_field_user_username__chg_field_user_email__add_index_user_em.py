# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'User.username'
        db.alter_column(u'people_user', 'username', self.gf('django.db.models.fields.SlugField')(max_length=32, primary_key=True))

        # Changing field 'User.email'
        db.alter_column(u'people_user', 'email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75))
        # Adding index on 'User', fields ['email']
        db.create_index(u'people_user', ['email'])


    def backwards(self, orm):
        # Removing index on 'User', fields ['email']
        db.delete_index(u'people_user', ['email'])


        # Changing field 'User.username'
        db.alter_column(u'people_user', 'username', self.gf('django.db.models.fields.SlugField')(max_length=32, unique=True))

        # Changing field 'User.email'
        db.alter_column(u'people_user', 'email', self.gf('django.db.models.fields.EmailField')(max_length=75, primary_key=True))

    models = {
        u'people.comment': {
            'Meta': {'object_name': 'Comment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'people.user': {
            'Meta': {'object_name': 'User'},
            'avatar': ('django.db.models.fields.files.ImageField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'db_index': 'True'}),
            'gender': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'join_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_online': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'username': ('django.db.models.fields.SlugField', [], {'max_length': '32', 'primary_key': 'True'})
        }
    }

    complete_apps = ['people']