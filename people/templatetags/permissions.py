from django import template

register = template.Library()

class Permission(object):
	def __init__(self, user, parent=None):
		print user
		self.user = user
		self.parent = parent
	
	def __getattr__(self,attr):
		p = Permission(self.user, '.'.join([self.parent,attr]) if self.parent else attr)
		call = p.__call__()
		if(call == None):
			return p
		else:
			return call
	
	def __call__(self):
		try:
			split_parent = self.parent.split(".")
			
			for i in xrange(len(split_parent),0,-1):
				try:
					module = __import__('.'.join(split_parent[:i]))
				except:
					continue
				break
			
			for m in split_parent[1:]:
				module = getattr(module,m)
				print module
			print module.can
			return module.can(self.user)
		except Exception as e:
			print e
			return None

class LoadPermissions(template.Node):
	def __init__(self,var_name):
		self.var_name = var_name
	
	def render(self, context):
		context[self.var_name] = Permission(context["user"])
		return u""

def load_permissions(parser, token):
	"""
		{% load_permissions at <var_name> %}
	"""
	parts = token.split_contents()
	
	if(len(parts) != 3 or parts[1] != 'at'):
		raise template.TemplateSyntaxError("'load_permissions' tag must be of the form:  {% load_permissions at <var_name> %}")
	
	return LoadPermissions(parts[2])

register.tag('load_permissions', load_permissions)
