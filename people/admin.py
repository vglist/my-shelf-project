from django.contrib import admin
from django import forms
from people.models import *

from django.core.files.uploadedfile import InMemoryUploadedFile

class UserChangeForm(forms.ModelForm):
	password1 = forms.CharField(label='New Password', widget=forms.PasswordInput,required=False)
	password2 = forms.CharField(label='Confirm New Password', widget=forms.PasswordInput,required=False)
	password_hash = forms.CharField(label='Password Hash', initial='pass_hash', help_text='No touching', widget=forms.TextInput(attrs={'readonly':'readonly'}), required=False)
	
	avatar = forms.ImageField(label='Avatar',required=False)
	
	def __init__(self, *args, **kwargs):
		super(UserChangeForm,self).__init__(*args, **kwargs)
		self.fields['password_hash'].initial = super(UserChangeForm,self).save(commit=False).password
	
	def clean_password2(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if (password1 or password2) and password1 != password2:
			raise forms.ValidationError("Passwords don't match")
		return password2
	
	def save(self, commit=True):
		user = super(UserChangeForm, self).save(commit=False)
		
		avatar = self.cleaned_data.get("avatar")
		
		if(isinstance(avatar,InMemoryUploadedFile)):
			user.save_images_from_raw(avatar.read())
		elif(avatar==False):
			user.avatar = None
		
		if(self.cleaned_data.get("password1")):
			user.set_password(self.cleaned_data["password1"])
		if commit:
			user.save()
		return user
	
	class Meta:
		model = User
		fields = ('email','username','gender','join_time','last_online','is_active')

class UserAdmin(admin.ModelAdmin):
	form = UserChangeForm

admin.site.register(User,UserAdmin)
admin.site.register(GroupMember)
admin.site.register(Comment)
