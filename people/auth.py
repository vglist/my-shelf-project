from django.contrib.auth.backends import ModelBackend
from django.utils import timezone

from people.models import *

class UserBackend(ModelBackend):
	def authenticate(self,email='',username='',password=''):
		try:
			user = User.objects.get(email=email)
		except User.DoesNotExist:
			try:
				user = User.objects.get(username__iexact=username)
			except User.DoesNotExist:
				User().set_password(password)
				return None
			else:
				if(user.check_password(password)):
					return user
				else:
					User().set_password(password)
					return None
		else:
			if(user.check_password(password)):
				return user
			else:
				User().set_password(password)
				return None
	
	def get_user(self, user_id):
		try:
			user = User.objects.get(pk=user_id)
		except User.DoesNotExist:
			return None
		else:
			user.last_online = timezone.now()
			user.save()
			return user
