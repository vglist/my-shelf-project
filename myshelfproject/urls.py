from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView

from myshelfproject import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='vglist/')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^vglist/', include('vglist.urls')),
    url(r'^ppl/', include('people.urls')),
    url(r'^actions/', include('action_confirm.urls')),
    url(r'^media/(?P<path>.*)$','django.views.static.serve',{'document_root': settings.MEDIA_ROOT, }),
)

handler404 = 'vglist.views.not_found'
