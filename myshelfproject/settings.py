"""
Django settings for myshelfproject project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import re
import shutil
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

try:
	from myshelfproject.local__settings import *
except ImportError:
	shutil.copyfile(os.path.join(os.path.dirname(__file__),'default_local__settings.py'),os.path.join(os.path.dirname(__file__),'local__settings.py'))
	
	print "Local settings not found. Creating new local__settings.py file at project directory."
	
	from myshelfproject.local__settings import *

try:
	DOMAIN_NAME
	EMAIL_BACKEND
	DEBUG
	LATENCY_MIDDLEWARE
except NameError as e:
	raise ImportError('%s not set in local__settings.py. Please do so before procedding.' % (e.message.split("'")[1]))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4!^+5td+b648&58p@%%g(j)80x8olgds!pqt#$oh@hhxlo6v^)'

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [
	'.vglist.net',
	'.vglist.org',
	'.vglist.info',
	'.vglist.beastarman.info',
]


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    
    'south',
    'django_extensions',
    
    'vglist',
    'people',
    
    'extensions',
    'action_confirm',
    
    'django_inaccurate_date',

    'preprocess',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    
    'latency.middleware.LatencyMiddleware',
    'preprocess.middleware.Recompile',
    'people.middleware.DoesNotHavePermHandler',
)

PREPROCESSOR_COMPILERS = (
    {
        "source": "resources/prep/css/",
        "target": "resources/static/css/",
        "compilers": (
			{
				"file": (
					"*.min.css",
				),
				"compiler": (
					"preprocess.builtin.Nothing",
				),
			},
			{
				"file": (
					"*.css",
				),
				"compiler": (
					"preprocess.builtin.DjangoTemplate",
					"preprocess.builtin.CSSMin"
				),
			},
		),
    },
    {
        "source": "resources/prep/js/",
        "target": "resources/static/js/",
        "compilers": (
			{
				"file": (
					"*.min.js",
				),
				"compiler": (
					"preprocess.builtin.Nothing",
				),
			},
			{
				"file": (
					"*.js",
				),
				"compiler": (
					"preprocess.builtin.DjangoTemplate",
					"preprocess.builtin.JSMin"
				),
			},
		),
    },
)

ROOT_URLCONF = 'myshelfproject.urls'

WSGI_APPLICATION = 'myshelfproject.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

MEDIA_URL = '/media/'

STATICFILES_DIRS = (
	os.path.join(BASE_DIR, 'resources/static/'),
)

ALLOWED_INCLUDE_ROOTS = (
	os.path.join(BASE_DIR, 'resources/static/'),
)

TEMPLATE_DIRS = (
	os.path.join(BASE_DIR, 'resources/templates/'),
)

AUTHENTICATION_BACKENDS = (
	'people.auth.UserBackend',
	'django.contrib.auth.backends.ModelBackend',
)

AUTH_USER_MODEL = 'people.User'

EMAIL_FILE_PATH = os.path.join(BASE_DIR, 'tmp/mail/')
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25
EMAIL_USE_TLS = False

MEDIA_ROOT = os.path.join(BASE_DIR,'resources/media/')

USER_GROUPS_PERMISSIONS = {
	'regular': ('add',),
	'news': ('post_news',),
}

#PEOPLE_NO_PERM_REDIRECT_VIEW = 'vglist.views.home'
#PEOPLE_NO_PERM_REDIRECT_ARGS = []
#PEOPLE_NO_PERM_REDIRECT_KWARGS = {}
