from django.shortcuts import render
from django.http import *
from action_confirm.models import *

def run(request,hash_id):
	try:
		a = Action.objects.get(hash_id=hash_id, public=True)
	except Action.DoesNotExist:
		return HttpResponseNotFound("404 - Not Found")
	else:
		return HttpResponseRedirect(a.run())

def cancel(request,hash_id):
	try:
		a = Action.objects.get(hash_id=hash_id, public=True)
	except Action.DoesNotExist:
		return HttpResponseNotFound("404 - Not Found")
	else:
		return HttpResponseRedirect(a.cancel())
