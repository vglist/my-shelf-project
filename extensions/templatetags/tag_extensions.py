from django import template

register = template.Library()

@register.filter(name='img_url')
def url(value):
	if(value):
		return value.url
	else:
		return ""
