from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

import pickle
import json
import copy
import os
import string
import random

from django.conf import settings

class AlternativeName(models.Model):
	name = models.CharField(blank=False, null=False, max_length=128)
	
	content_type = models.ForeignKey(ContentType)
	object_id = models.CharField(max_length=100)
	content_object = generic.GenericForeignKey('content_type', 'object_id')
	
	def __unicode__(self):
		return self.name

class EditStateM2M():
	def __init__(self,cur):
		self.cur = [i.pk if isinstance(i,models.Model) else i for i in cur.all()]
	
	def set(self,obj):
		self.cur = [i.pk if isinstance(i,models.Model) else i for i in obj]
	
	def add(self,i):
		self.cur.append(i.pk if isinstance(i,models.Model) else i)
	
	def remove(self,i):
		self.cur.remove(i.pk if isinstance(i,models.Model) else i)
	
	def clear(self):
		self.cur = []
	
	def all(self):
		return [i for i in self.cur];

class EditStateFileInputWrapper(object):
	BASE_UPLOAD_SUBPATH = 'edit_state/'
	BASE_HASH_LETTERS = string.ascii_letters+string.digits;
	
	def __init__(self, obj):
		self.__dict__['obj'] = copy.deepcopy(obj)
	
	def generate_temporary_file(self, data):
		filename = ''.join(random.choice(self.BASE_HASH_LETTERS) for _ in range(32))
		filepath = os.path.join(self.BASE_UPLOAD_SUBPATH,filename)
		
		f = open(os.path.join(settings.MEDIA_ROOT,filepath),"w+")
		f.write(data)
		f.close()
		
		self.__dict__['obj'].name = filepath
	
	def __getattr__(self,name):
		func = getattr(self.__dict__['obj'], name)
		
		if callable(func):
			def my_wrapper(*args, **kwargs):
				return func(*args, **kwargs)
			return my_wrapper
		else:
			return func
	
	def __setattr__(self,name,thing):
		setattr(self.__dict__['obj'], name, thing)

class EditStateWrapper(object):
	def __init__(self, obj):
		self.__dict__['obj'] = copy.deepcopy(obj)
		self.__dict__['m2m'] = {}
		self.__dict__['files'] = {}
		
		for m2m in obj.__class__._meta.many_to_many:
			self.__dict__['m2m'][m2m.name] = EditStateM2M(getattr(self.__dict__['obj'], m2m.name))
		
		for field in obj.__class__._meta.fields:
			if(isinstance(field,models.FileField)):
				self.__dict__['files'][field.name] = EditStateFileInputWrapper(getattr(self.__dict__['obj'], field.name))
	
	def __getattr__(self,name):
		if(name in self.__dict__['m2m']):
			return self.__dict__['m2m'][name]
		elif(name in self.__dict__['files']):
			return self.__dict__['files'][name]
		
		func = getattr(self.__dict__['obj'], name)
		
		if callable(func):
			def my_wrapper(*args, **kwargs):
				return func(*args, **kwargs)
			return my_wrapper
		else:
			return func
	
	def __setattr__(self,name,thing):
		if(name in self.__dict__['m2m']):
			self.__dict__['m2m'][name].set(thing)
			return;
		
		setattr(self.__dict__['obj'], name, thing)

class EditStateManager(models.Manager):
	def prepare_for_edit(self,obj):
		return EditStateWrapper(obj)

class EditState(models.Model):
	content_type = models.ForeignKey(ContentType)
	object_id = models.CharField(max_length=100)
	content_object = generic.GenericForeignKey('content_type', 'object_id')
	
	change = models.TextField()
	
	objects = EditStateManager()
	
	class Meta:
		abstract = True
	
	def __unicode__(self):
		return "Update on object %s" % (self.content_object.__unicode__(),)
	
	@staticmethod
	def get_diff_from_objects(original,new):
		diff = {'fields':{}, 'm2m':{}, 'files':{}}
		
		for field in original.__class__._meta.fields:
			if(isinstance(field,models.FileField)):
				if(getattr(original,field.name).name != getattr(new,field.name).name):
					diff['files'][field.name] = getattr(new,field.name).name
			elif(getattr(original,field.name) != getattr(new,field.name)):
				if(isinstance(field,models.ForeignKey)):
					diff['fields'][field.name] = field.value_from_object(new)
				else:
					diff['fields'][field.name] = pickle.dumps(getattr(new,field.name))
		
		for m2m in original.__class__._meta.many_to_many:
			original_m2m = [i.pk for i in getattr(original,m2m.name).all()]
			new_m2m = getattr(new,m2m.name).all()
			
			for i in new_m2m:
				if(i not in original_m2m):
					if(m2m.name not in diff['m2m']):
						diff['m2m'][m2m.name] = {'add':[]}
					diff['m2m'][m2m.name]['add'].append(i)
			
			for i in original_m2m:
				if(i not in new_m2m):
					if(m2m.name not in diff['m2m']):
						diff['m2m'][m2m.name] = {'rem':[]}
					elif('rem' not in diff['m2m'][m2m.name]):
						diff['m2m'][m2m.name]['rem'] = []
					diff['m2m'][m2m.name]['rem'].append(i)
		
		return json.dumps(diff)
