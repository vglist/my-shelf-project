# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'VGListComment'
        db.create_table(u'vglist_vglistcomment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('comment', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['people.Comment'], unique=True)),
            ('commenter', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vglist.VGListUser'])),
        ))
        db.send_create_signal(u'vglist', ['VGListComment'])


        # Changing field 'VGListUser.user'
        db.alter_column(u'vglist_vglistuser', 'user_id', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['people.User'], unique=True, primary_key=True))

    def backwards(self, orm):
        # Deleting model 'VGListComment'
        db.delete_table(u'vglist_vglistcomment')


        # Changing field 'VGListUser.user'
        db.alter_column(u'vglist_vglistuser', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.User'], primary_key=True))

    models = {
        u'people.comment': {
            'Meta': {'object_name': 'Comment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'post_time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'people.user': {
            'Meta': {'object_name': 'User'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'vglist.gameglobal': {
            'Meta': {'object_name': 'GameGlobal'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'vglist.gamelocal': {
            'Meta': {'ordering': "['game_global', 'local_id']", 'unique_together': "[['game_global', 'local_id']]", 'object_name': 'GameLocal', 'index_together': "[['game_global', 'local_id']]"},
            'console': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.Platform']", 'null': 'True', 'blank': 'True'}),
            'developers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'developed_games'", 'blank': 'True', 'to': u"orm['vglist.Studio']"}),
            'game_global': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.GameGlobal']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'publisher': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'published_games'", 'null': 'True', 'to': u"orm['vglist.Studio']"})
        },
        u'vglist.platform': {
            'Meta': {'object_name': 'Platform'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufacturer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.Studio']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'vglist.studio': {
            'Meta': {'object_name': 'Studio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'vglist.vglistcomment': {
            'Meta': {'object_name': 'VGListComment'},
            'comment': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['people.Comment']", 'unique': 'True'}),
            'commenter': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.VGListUser']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'vglist.vglistuser': {
            'Meta': {'object_name': 'VGListUser'},
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['people.User']", 'unique': 'True', 'primary_key': 'True'})
        }
    }

    complete_apps = ['vglist']