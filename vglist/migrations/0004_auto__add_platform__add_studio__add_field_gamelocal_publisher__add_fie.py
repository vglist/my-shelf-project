# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Platform'
        db.create_table(u'vglist_platform', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('synopsis', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('manufacturer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vglist.Studio'], null=True, blank=True)),
        ))
        db.send_create_signal(u'vglist', ['Platform'])

        # Adding model 'Studio'
        db.create_table(u'vglist_studio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('synopsis', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'vglist', ['Studio'])

        # Adding field 'GameLocal.publisher'
        db.add_column(u'vglist_gamelocal', 'publisher',
                      self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='published_games', null=True, to=orm['vglist.Studio']),
                      keep_default=False)

        # Adding field 'GameLocal.console'
        db.add_column(u'vglist_gamelocal', 'console',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vglist.Platform'], null=True, blank=True),
                      keep_default=False)

        # Adding M2M table for field developers on 'GameLocal'
        m2m_table_name = db.shorten_name(u'vglist_gamelocal_developers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('gamelocal', models.ForeignKey(orm[u'vglist.gamelocal'], null=False)),
            ('studio', models.ForeignKey(orm[u'vglist.studio'], null=False))
        ))
        db.create_unique(m2m_table_name, ['gamelocal_id', 'studio_id'])


    def backwards(self, orm):
        # Deleting model 'Platform'
        db.delete_table(u'vglist_platform')

        # Deleting model 'Studio'
        db.delete_table(u'vglist_studio')

        # Deleting field 'GameLocal.publisher'
        db.delete_column(u'vglist_gamelocal', 'publisher_id')

        # Deleting field 'GameLocal.console'
        db.delete_column(u'vglist_gamelocal', 'console_id')

        # Removing M2M table for field developers on 'GameLocal'
        db.delete_table(db.shorten_name(u'vglist_gamelocal_developers'))


    models = {
        u'vglist.gameglobal': {
            'Meta': {'object_name': 'GameGlobal'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'vglist.gamelocal': {
            'Meta': {'ordering': "['game_global', 'local_id']", 'unique_together': "[['game_global', 'local_id']]", 'object_name': 'GameLocal', 'index_together': "[['game_global', 'local_id']]"},
            'console': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.Platform']", 'null': 'True', 'blank': 'True'}),
            'developers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'developed_games'", 'blank': 'True', 'to': u"orm['vglist.Studio']"}),
            'game_global': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.GameGlobal']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'local_id': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'publisher': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'published_games'", 'null': 'True', 'to': u"orm['vglist.Studio']"})
        },
        u'vglist.platform': {
            'Meta': {'object_name': 'Platform'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufacturer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['vglist.Studio']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'vglist.studio': {
            'Meta': {'object_name': 'Studio'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'synopsis': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        }
    }

    complete_apps = ['vglist']