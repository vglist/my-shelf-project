from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
	url(r'^$', 'vglist.views.home'),
	url(r'^mypanel/$', 'vglist.views.mypanel'),
	
	url(r'^login/$', 'vglist.views.login'),
	url(r'^logout/$', 'vglist.views.logout'),
	url(r'^register/$', 'vglist.views.register'),
	url(r'^resend_activation_email/$', 'vglist.views.resend_activation_email'),
	
	url(r'^panel/$', 'vglist.views.panel'),
	url(r'^panel/create$', 'vglist.views.create_news'),
	url(r'^panel/drafts$', 'vglist.views.news_drafts'),
	url(r'^panel/(?P<news_id>\d+)/edit/$', 'vglist.views.edit_news'),	
	url(r'^panel/(?P<news_id>\d+)/delete/$', 'vglist.views.delete_news'),	
	
	url(r'^news/(?P<news_id>\d+)/$', 'vglist.views.view_news'),
	url(r'^news/all/$', 'vglist.views.view_all_news'),
	url(r'^news/(?P<news_id>\d+)/add_comment/$', 'vglist.views.add_comment_to_news'),
	
	url(r'^game/(?P<global_id>\d+)/$', 'vglist.views.game_global'),
	url(r'^game/(?P<global_id>\d+)/(?P<local_id>\d+)/$', 'vglist.views.game'),
	
	url(r'^studio/(?P<studio_id>\d+)/$', 'vglist.views.studio'),
	url(r'^studio/(?P<studio_id>\d+)/add_comment/$', 'vglist.views.add_comment_to_studio'),
		
	url(r'^profile/(?P<user_username>[a-zA-Z0-9_.-]+)/$', 'vglist.views.profile'),
	url(r'^profile/(?P<user_username>[a-zA-Z0-9_.-]+)/friends/$', 'vglist.views.friends'),
	url(r'^add_friend/(?P<user_username>[a-zA-Z0-9_.-]+)/$', 'vglist.views.add_friend'),
	url(r'^profile/(?P<user_username>[a-zA-Z0-9_.-]+)/add_comment/$', 'vglist.views.add_comment_to_profile'),

	url(r'^profile/(?P<user_username>[a-zA-Z0-9_.-]+)/list/$', 'vglist.views.user_list'),  #profile's game list
	url(r'^edit_preferences/$', 'vglist.views.edit_user'), #profile's user preferences
	url(r'^profile/(?P<user_username>[a-zA-Z0-9_.-]+)/notifications/$', 'vglist.views.notifications'),
	url(r'^profile/(?P<logged_user>[a-zA-Z0-9_.-]+)/(?P<user_username>[a-zA-Z0-9_.-]+)/accept_friend_request/$', 'vglist.views.accept_friend_request'), 
	url(r'^profile/(?P<logged_user>[a-zA-Z0-9_.-]+)/(?P<user_username>[a-zA-Z0-9_.-]+)/reject_friend_request/$', 'vglist.views.reject_friend_request'), 
	url(r'^profile/(?P<logged_user>[a-zA-Z0-9_.-]+)/(?P<user_username>[a-zA-Z0-9_.-]+)/remove_friend/$', 'vglist.views.remove_friend'), 
	
	url(r'^game/add/$', 'vglist.views.add_game'),
	url(r'^game/(?P<global_id>\d+)/add/$', 'vglist.views.add_game_version'),
	url(r'^game/(?P<global_id>\d+)/(?P<local_id>\d+)/edit/$', 'vglist.views.edit_game'),
	url(r'^game/(?P<global_id>\d+)/(?P<local_id>\d+)/add_comment/$', 'vglist.views.add_comment_to_game'),
	url(r'^game/(?P<global_id>\d+)/(?P<local_id>\d+)/add_list_entry/$', 'vglist.views.add_list_entry'),
	url(r'^game/(?P<global_id>\d+)/(?P<local_id>\d+)/edit_list_entry/$', 'vglist.views.edit_list_entry'),
	url(r'^game/(?P<global_id>\d+)/(?P<local_id>\d+)/remove_list_entry/$', 'vglist.views.remove_list_entry'),

	url(r'^platform/(?P<platform_id>\d+)/$', 'vglist.views.platform'),	
	url(r'^platform/add/$', 'vglist.views.add_platform'),
	url(r'^platform/(?P<platform_id>\d+)/edit/$', 'vglist.views.edit_platform'),
	url(r'^platform/(?P<platform_id>\d+)/add_comment/$', 'vglist.views.add_comment_to_platform'),
	
	url(r'^studio/add/$', 'vglist.views.add_studio'),
	url(r'^studio/(?P<studio_id>\d+)/edit/$', 'vglist.views.edit_studio'),

	url(r'^search/$', 'vglist.views.search'),
	
	url(r'^404/$', 'vglist.views.not_found'),
	url(r'^policy/$', 'vglist.views.policy'),
	url(r'^about/$', 'vglist.views.about'),
	url(r'^contact/$', 'vglist.views.contact'),
	url(r'^staff/$', 'vglist.views.staff'),
	
	url(r'^json/', include(patterns('vglist.views',
		url(r'^prefix/studio/$', 'studios_by_prefix'),
		url(r'^prefix/platform/$', 'platforms_by_prefix'),
	))),
	
	# TEST VIEW!!!! REMOVE ESSA MERDA BECKER!!!
	
	url(r'^request_recover_password/$', 'vglist.views.request_recover_password'),
	url(r'^recover_password/$', 'vglist.views.recover_password'),
)
